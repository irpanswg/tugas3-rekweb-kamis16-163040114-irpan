<?php

require APPPATH . '/libraries/REST_Controller.php';

class Mainan extends REST_Controller
{

    public function __construct($config = "rest")
    {
        parent::__construct($config);
        $this->load->model("Mainan_model", "mainan");
    }

    public function index_get()
    {
        $id = $this->get('id_produk');
        $cari = $this->get('cari');

        if ($cari != "") {
            $mainan = $this->mainan->cari($cari)->result();
        } else if ($id == "") {
            $mainan = $this->mainan->getData(null)->result();
        } else {
            $mainan = $this->mainan->getData($id)->result();
        }

        $this->response($mainan);
        
    }

    public function index_put()
    {
        $nama = $this->put('nama_produk');
        $deskripsi = $this->put('deskripsi');
        $harga = $this->put('harga');
        $stok = $this->put('stok');
        $kategori = $this->put('kategori');
        $data = array(
            "nama_produk" => $nama,
            "deskripsi" => $deskripsi,
            "stok" => $stok,
            "kategori" => $kategori,
        );
        $update = $this->mainan->update('tbl_produk', $data, 'id_produk', $this->put('id_produk'));

        if ($update) {
            $this->response(array('status' => 'success', 200, 'data' => $data));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
        
    }

    public function index_post()
    {
        $id = $this->post('id_produk');
        $nama = $this->post('nama_produk');
        $deskripsi = $this->post('deskripsi');
        $harga = $this->post('harga');
        $stok = $this->post('stok');
        $kategori = $this->post('kategori');


        $data = array(
            "id_produk" => $id,
            "nama_produk" => $nama,
            "deskripsi" => $deskripsi,
            "harga" => $harga,
            "stok" => $stok,
            "kategori" => $kategori,        
        );

        $insert = $this->mainan->insert($data);

        if ($insert) {
            $this->response(array('status' => 'success', 200, 'data' => $data));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function index_delete()
    {
        $id = $this->delete('id_produk');
        // $this->db->where('id', $id);
        $delete = $this->mainan->delete('tbl_produk', 'id_produk', $id);

        if ($delete) {
            $this->response(array('status' => 'success', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}